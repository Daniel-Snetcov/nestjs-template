{
  description = "Flake to manage build a nestjs appliation and manage development dependencies";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
  };

  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;}
    {
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];
      perSystem = {pkgs, ...}: let
        nodejs = pkgs.nodejs_22;
      in {
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = [
            nodejs
            nodejs.pkgs.pnpm
          ];
        };
      };
    };
}
