export function DecorateAll(decorator: MethodDecorator): ClassDecorator {
  return (target) => {
    const descriptors = Object.getOwnPropertyDescriptors(target.prototype);

    for (const [propertyKey, descriptor] of Object.entries(descriptors)) {
      const isMethod = typeof descriptor.value === 'function'
                && propertyKey !== 'constructor';
      if (isMethod) {
        decorator(target, propertyKey, descriptor);

        Object.defineProperty(target.prototype, propertyKey, descriptor);
      }
    }
  };
}
