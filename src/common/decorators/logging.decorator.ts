/* eslint-disable no-param-reassign */
/* eslint-disable func-names */
/* eslint-disable @typescript-eslint/ban-types */
import { isPromise } from 'util/types';

import { copyMetadata, getCircularReplacer } from '@common/utils';
import { HttpStatus, Logger } from '@nestjs/common';
import { isObservable, tap } from 'rxjs';

import { RuntimeException } from '../filters/runtime-exception.filter';

import { DecorateAll } from './decorate-all.decorator';

import type { LogContext } from '../types';
import type { Observable } from 'rxjs';

interface Metadata {
  context: LogContext
}

function isPlainError(error: unknown): error is Error {
  if (!error) {
    return false;
  }

  return Object.getPrototypeOf(error) === Error.prototype;
}

function handleError(error: unknown, { context }: Metadata): void {
  if (isPlainError(error)) {
    const runtimeException = new RuntimeException({
      name: RuntimeException.name,
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      message: error.message,
      isPropagated: true,
      stackTrace: [context],
    });
    Logger.error(error, context);
    throw runtimeException;
  }
  if (RuntimeException.isIstance(error)) {
    if (!error.isPropagated) {
      error.isPropagated = true;
      Logger.error(error, context);
    }
    error.stackTrace.push(context);
    throw error;
  }

  throw error;
}

async function logPromise(
  result: Promise<unknown>,
  metadata: Metadata,
): Promise<unknown> {
  return result
    .then((data) => {
      Logger.debug(
        `[Finish] => ${JSON.stringify(
          {
            result: data,
          },
          getCircularReplacer(),
        )}`,
        metadata.context,
      );
      return data;
    })
    .catch((error: unknown) => {
      handleError(error, metadata);
    });
}

function logObservable(
  source$: Observable<unknown>,
  { context }: Metadata,
): Observable<unknown> {
  return source$.pipe(
    tap({
      next: (value) => {
        Logger.debug(
          `[Finish] => ${JSON.stringify(
            {
              result: value,
            },
            getCircularReplacer(),
          )}`,
          context,
        );
      },
      error: (error: Error) => {
        handleError(error, { context });
      },
    }),
  );
}

function logResult(result: unknown, metadata: Metadata): unknown {
  if (isPromise(result)) {
    return logPromise(result, metadata);
  }
  if (isObservable(result)) {
    return logObservable(result, metadata);
  }
  Logger.debug(
    `[Finish] => ${JSON.stringify(
      {
        result,
      },
      getCircularReplacer(),
    )}`,
    metadata.context,
  );

  return result;
}

function LoggingInterceptor(
  target: object,
  method: string | symbol,
  descriptor: PropertyDescriptor,
): PropertyDescriptor {
  let service = target.constructor.name;
  if (typeof target === 'function') {
    service = target.name;
  }

  const context: Metadata['context'] = `${service}::${method.toString()}`;
  const originalMethod: unknown = descriptor.value;
  if (!originalMethod || !(originalMethod instanceof Function)) {
    return descriptor;
  }

  descriptor.value = function (...args: unknown[]): unknown {
    let result: unknown;
    Logger.debug(
      `[Start] => ${JSON.stringify({ args }, getCircularReplacer())}`,
      context,
    );
    try {
      result = originalMethod.apply(this, args);
      result = logResult(result, {
        context,
      });
    } catch (error: unknown) {
      handleError(error, { context });
    }

    return result;
  };

  copyMetadata(originalMethod, descriptor.value as Function);
  return descriptor;
}

export function LogMethod(): MethodDecorator {
  return LoggingInterceptor;
}

export function LogMethods(): ClassDecorator {
  return DecorateAll(LogMethod());
}
