/* eslint-disable no-param-reassign */
/* eslint-disable func-names */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { HttpStatus, Logger } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validateSync } from 'class-validator';

import { RuntimeException } from '../filters';

import { DecorateAll } from './decorate-all.decorator';

import type { ClassConstructor } from 'class-transformer';

function Serialize(
  target: object,
  key: string | symbol,
  descriptor: PropertyDescriptor,
): PropertyDescriptor {
  let service = target.constructor.name;
  if (typeof target === 'function') {
    service = target.name;
  }
  const method: unknown = descriptor.value;
  const context = `${service}::${key.toString()}`;
  const paramTypes: unknown[] = Reflect.getMetadata('design:paramtypes', target, key);
  if (!method || !(method instanceof Function)) {
    return descriptor;
  }
  descriptor.value = function (...args: unknown[]): unknown {
    const serializedArgs = args
      .map((arg, index) => {
        return plainToInstance(paramTypes[index] as ClassConstructor<object>, arg);
      });
    for (const arg of serializedArgs) {
      const errors = validateSync(arg);
      if (errors.length) {
        for (const error of errors) {
          Logger.error(`Parameters validation failed: ${error.toString()}`, context);
        }
        throw new RuntimeException({
          name: RuntimeException.name,
          message: '🌀 Oops! The Application Had a "Philosophical Debate"',
          isPropagated: true,
          stackTrace: [context],
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        });
      }
    }
    return method.apply(this, serializedArgs) as unknown;
  };
  return descriptor;
}

export function SerializeMethod(): MethodDecorator {
  return Serialize;
}

export function SerializeMethods(): ClassDecorator {
  return DecorateAll(SerializeMethod());
}
