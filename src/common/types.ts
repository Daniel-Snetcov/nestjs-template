type ClassName = string;
type CalledMethod = string;

export type LogContext = `${ClassName}::${CalledMethod}`;
