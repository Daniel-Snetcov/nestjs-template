export function copyMetadata(from: object, target: object): void {
  const metadataKeys = Reflect.getMetadataKeys(from);

  for (const key of metadataKeys) {
    const value: unknown = Reflect.getMetadata(key, from);

    Reflect.defineMetadata(key, value, target);
  }
}
