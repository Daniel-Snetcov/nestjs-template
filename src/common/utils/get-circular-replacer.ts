const FIELDS_TO_HIDE = ['authorization', 'auth', 'token'];

export function getCircularReplacer(): (
  _key: string,
  value: unknown
) => unknown {
  const ancestors: unknown[] = [];
  // eslint-disable-next-line func-names
  return function (this: unknown, key: string, value: unknown): unknown {
    if (FIELDS_TO_HIDE.includes(key.toLowerCase())) {
      return '[Hidden]';
    }
    if (typeof value !== 'object' || value === null) {
      return value;
    }
    while (ancestors.length > 0 && ancestors.at(-1) !== this) {
      ancestors.pop();
    }
    if (ancestors.includes(value)) {
      return '[Circular]';
    }
    ancestors.push(value);

    return value;
  };
}
