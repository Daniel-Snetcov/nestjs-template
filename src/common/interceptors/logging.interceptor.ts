import { LogContext } from '@common/types';
import { getCircularReplacer } from '@common/utils';
import { Injectable, Logger } from '@nestjs/common';
import { tap } from 'rxjs';

import type {
  CallHandler,
  ExecutionContext,
  NestInterceptor,
} from '@nestjs/common';
import type { Request, Response } from 'express';
import type { Observable } from 'rxjs';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  public intercept(
    context: ExecutionContext,
    next: CallHandler<unknown>,
  ): Observable<unknown> {
    const { method, url, body } = context
      .switchToHttp()
      .getRequest<Request<unknown, unknown, unknown>>();
    const { statusCode } = context.switchToHttp().getResponse<Response>();
    const logContext: LogContext = `${method}::${url}`;

    Logger.debug(
      `[Start] => ${JSON.stringify({ body }, getCircularReplacer())}`,
      logContext,
    );
    return next.handle().pipe(
      tap({
        next: (result) => {
          Logger.debug(
            `[Finish] => ${JSON.stringify({ result, statusCode }, getCircularReplacer())}`,
            logContext,
          );
        },
      }),
    );
  }
}
