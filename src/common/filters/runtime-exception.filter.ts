import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Response } from 'express';

export class RuntimeException extends Error {
  statusCode: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

  stackTrace: string[] = [];

  isPropagated = false;

  constructor(options: RuntimeException) {
    super();
    Object.assign(this, options);
  }

  static isIstance(error: unknown): error is RuntimeException {
    if (!error) {
      return false;
    }
    return Object.getPrototypeOf(error) === RuntimeException.prototype;
  }
}

@Catch(RuntimeException)
export class RuntimeExceptionFilter
implements ExceptionFilter<RuntimeException> {
  private readonly logger = new Logger();

  catch(exception: RuntimeException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    this.logger.error(
      JSON.stringify({
        message: exception.message,
      }),
      exception.stackTrace.join(' => '),
    );

    response.status(exception.statusCode).json({
      message: exception.message,
    });
  }
}
