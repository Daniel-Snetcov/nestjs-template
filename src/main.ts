import { AppEnvVariables } from '@core/config';
import { AppModule } from '@modules';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import type { LogContext } from '@common/types';

const LOG_CONTEXT: LogContext = 'NestApplication::Boostrap';
async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  const variables = AppEnvVariables.getVariables();

  await app.listen(variables.PORT);

  const url = await app.getUrl();
  Logger.log(`Nest application successfully started on: ${url}`, LOG_CONTEXT);
}

bootstrap().catch((error: unknown) => {
  Logger.fatal(error, LOG_CONTEXT);
  process.exit(1);
});
