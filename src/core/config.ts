import { Logger, type LogLevel } from '@nestjs/common';
import { Expose, plainToInstance, Transform } from 'class-transformer';
import {
  IsIn, IsInt, IsNotEmpty, validateSync,
} from 'class-validator';

import type { ConfigModuleOptions } from '@nestjs/config';

export const ENVIRONMENTS = ['local', 'dev', 'prod'] as const satisfies string[];
export type Environment = (typeof ENVIRONMENTS)[number];

export const LOG_LEVELS = [
  'log',
  'error',
  'warn',
  'debug',
  'verbose',
  'fatal',
] as const satisfies LogLevel[];

export class AppEnvVariables {
  @IsIn(ENVIRONMENTS)
  @IsNotEmpty()
  @Expose()
    ENV!: Environment;

  @Transform(({ value }) => {
    return +value;
  })
  @IsInt()
  @IsNotEmpty()
  @Expose()
    PORT!: number;

  @IsIn(LOG_LEVELS, { each: true })
  @Transform(({ value }) => {
    return String(value).split(',');
  })
  @IsNotEmpty()
  @Expose()
    LOG_LEVELS!: LogLevel[];

  static getVariables(): AppEnvVariables {
    return plainToInstance(AppEnvVariables, process.env, {
      excludeExtraneousValues: true,
    });
  }
}

export const CONFIG_MODULE_OPTIONS = {
  isGlobal: true,
  validate: () => {
    const envVariables = plainToInstance(AppEnvVariables, process.env);
    const errors = validateSync(envVariables);
    if (errors.length) {
      for (const { property, value, constraints } of errors) {
        const logPayload = {
          property,
          value: value as unknown,
          constraints,
        };
        Logger.fatal(
          `[Error] => ${JSON.stringify(logPayload, undefined, 2)}`,
          'Bootstrap::EnvValidation',
        );
      }
      throw new Error('Environment variables validation failed');
    }

    return envVariables;
  },
} as const satisfies ConfigModuleOptions;
