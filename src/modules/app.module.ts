import { RuntimeExceptionFilter } from '@common/filters';
import { LoggingInterceptor } from '@common/interceptors';
import { CONFIG_MODULE_OPTIONS } from '@core/config';
import { HealthModule } from '@core/health';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';

@Module({
  imports: [ConfigModule.forRoot(CONFIG_MODULE_OPTIONS), HealthModule],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: RuntimeExceptionFilter,
    },
  ],
})
export class AppModule {}
